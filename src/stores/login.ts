import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", () => {
  const username = ref("");
  const isLogin = computed(() => {
    return username.value !== "";
  });
  const login = (user: string): void => {
    username.value = user;
    localStorage.setItem("username", user);
  };
  const logout = (): void => {
    username.value = "";
    localStorage.removeItem("username");
  };
  const loadData = () => {
    username.value = localStorage.getItem("username") || "";
  };
  return { username, isLogin, login, logout, loadData };
});
